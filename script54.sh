#!/bin/bash

# Get the current date
date=$(date)

# Get the last 10 users who logged into the server
last_10_users=$(last -10)

# Get the swap space information
swap_space=$(free -h | grep Swap)

# Get the kernel version
kernel_version=$(uname -r)

# Get the IP address
ip_address=$(ip a | grep 'inet 10.*')

# Output the information
echo "Date: $date" > /tmp/serverinfo.info
echo "Last 10 Users: $last_10_users" >> /tmp/serverinfo.info
echo "Swap Space: $swap_space" >> /tmp/serverinfo.info
echo "Kernel Version: $kernel_version" >> /tmp/serverinfo.info
echo "IP Address: $ip_address" >> /tmp/serverinfo.info
